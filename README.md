# vue3-demo

---


### 目录索引

1. src   源代码目录
    - api       接口
    - request   请求拦截
    - routers   路由
    - view      页面
    - App.vue   首页
    - main.js   导入所有的包

## 添加依赖

```
npm install
```

### 运行

```
npm run serve
```

### 构建

```
npm run build
```

### Lints and fixes files

```
npm run lint
```

### Customize configuration

See [Configuration Reference](https://cli.vuejs.org/config/).
