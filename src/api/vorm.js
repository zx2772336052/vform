import httprequest from "@/request/httprequest";

 
///添加form表单
export async function addform(params) {
    return await httprequest.postJson('/addformtemplate', params);
}

///添加form表单
export async function getVformOne(params) {
    return await httprequest.get('/getvformone/'+ params);
}

///添加form表单下拉框选项
export async function getoptiondata() {
    return await httprequest.get('/getoptiondata');
}

///添加form表单下拉框选项
export async function getformtemplate() {
    return await httprequest.get('/getformtemplate');
}


//其实，也不一定就是params，也可以是 query 还有 data 的呀！
//params是添加到url的请求字符串中的，用于get请求。会将参数加到 url后面。所以，传递的都是字符串。无法传递参数中含有json格式的数据
//而data是添加到请求体（body）中的， 用于post请求。添加到请求体（body）中，json 格式也是可以的。