import { createApp } from 'vue'
import App from './App.vue'
import router from './routers/routes'
//import axiosr from "axios";

import ElementPlus from 'element-plus'  //引入element-plus库
import 'element-plus/dist/index.css'  //引入element-plus样式

import VForm3 from '@/../lib/vform/designer.umd.js'
import '../lib/vform/designer.style.css'

const app = createApp(App)//app.use(axiosr)
app.use(ElementPlus)  //全局注册element-plus
app.use(VForm3)  //全局注册VForm3，同时注册了v-form-designer、v-form-render等组件
app.use(router)
app.mount('#app')