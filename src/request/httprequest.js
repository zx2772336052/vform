import axios from 'axios'
import qs from 'qs'
import { ElMessage } from 'element-plus'
 


axios.defaults.headers['Content-Type'] = 'application/json';
// 创建 axios 请求实例
const serviceAxios = axios.create({
  baseURL: 'http://localhost:8090/api/', // 基础请求地址
  timeout: 5000, // 请求超时设置
  withCredentials: false, // 跨域请求是否需要携带 cookie
  
});


serviceAxios.interceptors.request.use(config => {
    // const token = localStorage.getItem('token');
    // if (token) {
    //     config.headers['Authorization'] = 'Bearer ' + token
    // }
    return config
}, error => {
    return new Promise.reject(error)
});
 
/**
 * axios 响应过滤器
 */
serviceAxios.interceptors.response.use(response => {
    if (response.data.code != 200) {
      Responsecode(response.data);
    };
    return response.data;
}, error => {
  Responsecode(error.data);
  /***** 处理结束 *****/
  //如果不需要错误处理，以上的处理过程都可省略
  return Promise.resolve(error.data)
});
 

function Responsecode(res){
      let message=res.msg;
      // 1.公共错误处理
      // 2.根据响应码具体处理
      switch (res.code) {
        case 400:
          message = '错误请求'
          break;
        case 401:
          message = '未授权，请重新登录'
          break;
        case 403:
          message = '拒绝访问'
          break;
        case 404:
          message = '请求错误,未找到该资源'
          window.location.href = "/NotFound"
          break;
        case 405:
          message = '请求方法未允许'
          break;
        case 408:
          message = '请求超时'
          break;
        case 500:
          message = '服务器端出错'
          break;
        case 501:
          message = '网络未实现'
          break;
        case 502:
          message = '网络错误'
          break;
        case 503:
          message = '服务不可用'
          break;
        case 504:
          message = '网络超时'
          break;
        case 505:
          message = 'http版本不支持该请求'
          break;
      };
      ElMessage.error(message);
}
/**
 * axios 封装请求
 */
export default {
    get(url, params = {}) {
        return serviceAxios.get(url, {
            params: params
        })
    },
    post(url, params = {}) {
        return serviceAxios.post(url, qs.stringify(params))
    },
    put(url, params = {}) {
        return serviceAxios.put(url, qs.stringify(params))
    },
    delete(url, params = {}) {
        return serviceAxios.delete(url, {
            params: params
        })
    },
    postJson(url, params) {
      return serviceAxios.post(url, params)
    },
}