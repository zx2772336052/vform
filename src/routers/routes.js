import {createRouter, createWebHistory} from "vue-router"

 
const routes = [
    {
        id:101,
        path: '/',
        name:'低代码模版',
        component: () => import('@/view/vForm/Designer.vue')
    },
    {
        id:103,
        path: '/r',
        name: '低代码解析',
        component: () => import('@/view/vForm/Render.vue')
    },
];
 
const router = createRouter({
    history: createWebHistory(),
    routes,
});
 
export default router
